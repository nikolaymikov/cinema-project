package cinema.classes;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CinemaHallTest {

    private CinemaHall cinemaHall;

    /**
     * setUp Method for initialisation of the CinemaHall class
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        cinemaHall = new CinemaHall("44");
    }


    /**
     * Get hallNumber test
     */
    @Test
    public void getHallNumber() {
        assertEquals("44",cinemaHall.getHallNumber());
    }

}