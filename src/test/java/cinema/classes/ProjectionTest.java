package cinema.classes;

import org.junit.Before;
import org.junit.Test;
import java.util.Date;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.Assert.*;

public class ProjectionTest {

    private Projection projection;
    @Before
    public void setUp() throws Exception {
        Movie movie = new Movie("Superman");
        CinemaHall hallNumber = new CinemaHall("5");
        projection = new Projection(movie,"3D");
        projection.setProjectionTimeStart("10-03-2018 19:50");
        projection.setProjectionHall(hallNumber);

    }

    @Test
    public void getProjectionTimeStart() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        try {
            Date testDate = dateFormat.parse("10-03-2018 19:50");
            assertEquals(testDate,projection.getProjectionTimeStart());
        }
        catch (ParseException e){
            e.printStackTrace();
        }
    }

    @Test
    public void getProjectionMovie() {
        assertEquals("Superman",projection.getProjectionMovie().getMovieName());
    }

    @Test
    public void getProjectionType() {
        assertEquals("3D",projection.getProjectionType());
    }

    @Test
    public void getProjectionHall() {
        assertEquals("5",projection.getProjectionHall().getHallNumber());
    }

    @Test
    public void getProjectionMovieFull() {
        assertEquals("Superman",projection.getProjectionMovieFull());
    }

    @Test
    public void getProjectionHallFull() {
        assertEquals("5",projection.getProjectionHallFull());
    }
}