package cinema.classes;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MovieTest {

    private Movie movie;
    @Before
    public void setUp() throws Exception {
        movie = new Movie("Spiderman");
        movie.setMovieGenre("Action");
        movie.setMovieYear("1992");
    }

    @Test
    public void getMovieName() {
        assertEquals("Spiderman",movie.getMovieName());
    }

    @Test
    public void getMovieDescription() {
        assertNull(null,movie.getMovieDescription());
    }

    @Test
    public void getMovieGenre() {
        assertEquals("Action",movie.getMovieGenre());
    }

    @Test
    public void getMovieYear() {
        assertEquals("1992",movie.getMovieYear());
    }

    @Test
    public void getMovieActors() {
        assertNull(null,movie.getMovieActors());
    }

    @Test
    public void getMovieTime() {
        assertNull(null,movie.getMovieTime());
    }


}