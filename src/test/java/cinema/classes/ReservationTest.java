package cinema.classes;

import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class ReservationTest {

    private Reservation reservation;
    private User user;
    private Movie movie;
    private Projection projection;
    @Before
    public void setUp() throws Exception {

        user = new User("Nikolay","Mikov","088123123");
        movie = new Movie("Batman");
        projection = new Projection(movie,"2D");
        projection.setProjectionTimeStart("11-03-2019 18:50");
        reservation = new Reservation(projection,"55",user);
        reservation.setReservationTime("11-03-2019 18:00");

    }

    @Test
    public void getProjection() {
        assertEquals(projection,reservation.getProjection());
    }

    @Test
    public void getReservationTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        try {
            Date testDate = dateFormat.parse("11-03-2019 18:00");
            assertEquals(testDate,reservation.getReservationTime());
        }
        catch (ParseException e){
            e.printStackTrace();
        }
    }

    @Test
    public void getReservationSeat() {
        assertEquals("55",reservation.getReservationSeat());
    }

    @Test
    public void getReservationUser() {
        assertEquals(user,reservation.getReservationUser());
    }


    @Test
    public void checkIfIsOnTime() {
        assertFalse(reservation.checkIfIsOnTime());
    }

    @Test
    public void checkIfSeatIsTaken() {
        assertTrue(reservation.checkIfSeatIsTaken("55"));
    }
}