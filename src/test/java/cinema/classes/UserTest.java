package cinema.classes;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {

    private User user;

    @Before
    public void setUp() throws Exception {
        user = new User("Nikolay","Mikov","0881232324");
    }

    @Test
    public void getFirstName() {
        assertEquals("Nikolay",user.getFirstName());
    }

    @Test
    public void getLastName() {
        assertEquals("Mikov",user.getLastName());
    }

    @Test
    public void getEmail() {
        assertNull(null,user.getEmail());
    }

    @Test
    public void getPhone() {
        assertEquals("0881232324",user.getPhone());
    }
}