import cinema.classes.*;

public class MainClass {

    public static void main(String[] args) {

        CinemaHall cinemaHall = new CinemaHall("3");

        Movie movie = new Movie("Star Wars");

        User user = new User("Ivan","Ivanov","088123123");


        Projection projection = new Projection(movie,"3D");
        projection.setProjectionHall(cinemaHall);
        projection.setProjectionTimeStart("08-03-2019 17:50");


        Reservation reservation  = new Reservation(projection,"43",user);
        reservation.setReservationTime("08-03-2019 16:50");
        System.out.println(reservation.checkIfIsOnTime());
        System.out.println(reservation.checkIfSeatIsTaken("43"));

        //Avoiding Train Wreck
        System.out.println("The full name of the projection Movie is: "+projection.getProjectionMovieFull());
        System.out.println("The hall number is: "+projection.getProjectionHallFull());

    }
}
