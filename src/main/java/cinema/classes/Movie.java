package cinema.classes;

/**
 * This class is constructing the Movie object.
 *
 * @author Nikolay Mikov
 */

public class Movie {

    private String movieName;
    private String movieDescription;
    private String movieGenre;
    private String movieYear;
    private String movieActors;
    private String movieTime;


    /**
     * @param movieName is expected by the constructor
     */
    public Movie(String movieName) {
        this.movieName = movieName;
    }

    /**
     * @return String movieName
     */
    public String getMovieName() {
        return movieName;
    }

    /**
     * @param movieName
     */
    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    /**
     * @return String movieDescription
     */
    public String getMovieDescription() {
        return movieDescription;
    }

    public void setMovieDescription(String movieDescription) {
        this.movieDescription = movieDescription;
    }

    /**
     * @return String
     */
    public String getMovieGenre() {
        return movieGenre;
    }

    /**
     * @param movieGenre
     */
    public void setMovieGenre(String movieGenre) {
        this.movieGenre = movieGenre;
    }

    /**
     * @return String
     */
    public String getMovieYear() {
        return movieYear;
    }

    /**
     * @param movieYear
     */
    public void setMovieYear(String movieYear) {
        this.movieYear = movieYear;
    }

    /**
     * @return String
     */
    public String getMovieActors() {
        return movieActors;
    }

    /**
     * @param movieActors
     */
    public void setMovieActors(String movieActors) {
        this.movieActors = movieActors;
    }

    /**
     * @return String
     */
    public String getMovieTime() {
        return movieTime;
    }

    /**
     * @param movieTime
     */
    public void setMovieTime(String movieTime) {
        this.movieTime = movieTime;
    }
}
