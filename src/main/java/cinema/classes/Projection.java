package cinema.classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class is constructing the Projection object
 *
 * @author Nikolay Mikov
 */

public class Projection {

    private Date projectionTimeStart;
    private Movie  projectionMovie;
    private String projectionType;
    private CinemaHall projectionHall;

    /**
     * @param projectionMovie
     * @param projectionType
     */
    public Projection(Movie projectionMovie, String projectionType) {
        this.projectionMovie = projectionMovie;
        this.projectionType = projectionType;
    }

    /**
     * @return Date
     */
    public Date getProjectionTimeStart() {
        return projectionTimeStart;
    }

    /**
     * @param projectionTimeStart
     *
     * This method is converting
     * a given String Date to
     * Date Object
     */
    public void setProjectionTimeStart(String projectionTimeStart) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        try {

            this.projectionTimeStart = dateFormat.parse(projectionTimeStart);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
    }


    /**
     * @return Movie
     */
    public Movie getProjectionMovie() {
        return projectionMovie;
    }

    /**
     * @param projectionMovie
     */
    public void setProjectionMovie(Movie projectionMovie) {
        this.projectionMovie = projectionMovie;
    }

    /**
     * @return String
     */
    public String getProjectionType() {
        return projectionType;
    }

    /**
     * @param projectionType
     */
    public void setProjectionType(String projectionType) {
        this.projectionType = projectionType;
    }

    /**
     * @return CinemaHall
     */
    public CinemaHall getProjectionHall() {
        return projectionHall;
    }

    /**
     * @param projectionHall
     */
    public void setProjectionHall(CinemaHall projectionHall) {
        this.projectionHall = projectionHall;
    }

    /**
     * Getting the Full name of the movie from
     * the Movie object
     *
     * @return String
     */
    public String getProjectionMovieFull(){
        return this.projectionMovie.getMovieName();
    }

    /**
     * Getting the projectionHall from the CinemaHall Object
     * @return String
     */
    public String getProjectionHallFull(){
        return this.projectionHall.getHallNumber();
    }


}
