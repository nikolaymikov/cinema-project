package cinema.classes;

/**
 * This class is constructing the CinemaHall Object
 *
 * @author Nikolay Mikov
 *
 */

public class CinemaHall {

    /**
     * Variable that holds the hallNumber
     * */
    private String hallNumber;

    /**
     * @param hallNumber This is the hallNumber which the Constructor expects
     */
    public CinemaHall(String hallNumber) {
        this.hallNumber = hallNumber;
    }

    /**
     * A getter method for the hallNumber
     * @return String
     */
    public String getHallNumber() {
        return hallNumber;
    }

    /**
     *  A setter  method
     * @param hallNumber a setter for the hallNumber
     */
    public void setHallNumber(String hallNumber) {
        this.hallNumber = hallNumber;
    }
}
