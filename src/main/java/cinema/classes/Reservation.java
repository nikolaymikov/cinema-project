package cinema.classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This Class is constructing the Reservation object
 * and creates the Reservation instance
 *
 * @author Nikolay Mikov
 */
public class Reservation {

    private Projection projection;
    private  Date  reservationTime;
    private  String reservationSeat;
    private  User reservationUser;
    private static final int SIXTY_MINUTES = 60 * 60 * 1000;

    /**
     * @param projection
     * @param reservationSeat
     * @param reservationUser
     */
    public Reservation(Projection projection, String reservationSeat, User reservationUser) {
        this.projection = projection;
        this.reservationSeat = reservationSeat;
        this.reservationUser = reservationUser;
    }

    /**
     * @return Projection
     */
    public Projection getProjection() {
        return projection;
    }

    /**
     * @param projection
     */
    public void setProjection(Projection projection) {
        this.projection = projection;
    }

    /**
     * @return Date
     */
    public Date getReservationTime() {
        return reservationTime;
    }

    /**
     *  This method is converting
     *  a given String Date to
     *  Date Object
     * @param reservationTime
     */
    public void setReservationTime(String reservationTime) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        try {

            this.reservationTime = dateFormat.parse(reservationTime);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

    }

    /**
     * @return String
     */
    public String getReservationSeat() {
        return reservationSeat;
    }

    /**
     * @param reservationSeat
     */
    public void setReservationSeat(String reservationSeat) {
        this.reservationSeat = reservationSeat;
    }

    /**
     * @return User
     */
    public User getReservationUser() {
        return reservationUser;
    }

    /**
     * @param reservationUser
     */
    public void setReservationUser(User reservationUser) {
        this.reservationUser = reservationUser;
    }

    /**
     * <p>This method checks if the User made the reservation
     * at least 60 minutes earlier than
     * the beginning of the projection time.</p>
     */
    public boolean checkIfIsOnTime() {
        Date projectionTime = projection.getProjectionTimeStart();

       long timeDifference = projectionTime.getTime() - this.reservationTime.getTime();

       if(timeDifference >= SIXTY_MINUTES) {

            System.out.println("The reservation is on time !");
            return true;
        }
        else {

            System.out.println("The reservation is NOT on time !");
            return false;
        }

    }

    /**
     * <p>This method is checking if the reservation seat
     * is already taken by another user.</p>
     *
     * @param reservationSeat
     * @return Boolean
     */
    public boolean checkIfSeatIsTaken(String reservationSeat) {

       if(this.reservationSeat.equals(reservationSeat)) {

         System.out.println("The seat is taken !");
         return true;
       }
       else{

           System.out.println("The seat is not taken !");
           return false;
       }
    }
}
